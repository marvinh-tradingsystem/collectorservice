#!/bin/bash
if [ -z "$DATABASE_URL" ]; then
    echo "Error: \$DATABASE_URL is not set"
    exit 1
fi

if [ -z "$QUERY_URL" ]; then
    echo "Error: \$QUERY_URL is not set"
    exit 1
fi

if [ -z "$PLUGIN_FILE" ]; then
    echo "Error: \$PLUGIN_FILE is not set"
    exit 1
fi

if [ -z "$PRODUCT_FILE" ]; then
    echo "Error: \$PRODUCT_FILE is not set"
    exit 1
fi

if [ -z "$CURRENCY_FILE" ]; then
    echo "Error: \$CURRENCY_FILE is not set"
    exit 1
fi

application_arguments=(
"--database.url=$DATABASE_URL"
"--query.url=$QUERY_URL"
"--plugin.file=$PLUGIN_FILE"
"--product.file=$PRODUCT_FILE"
"--currency.file=$CURRENCY_FILE"
)

if [ -n "$COLLECTOR_TYPE" ]; then
    application_arguments+=("--collector.type=$COLLECTOR_TYPE")
    echo "Added collector type to application arguments"
fi

if [ -n "$POLL_INTERVAL" ]; then
    application_arguments+=("--poll.interval=$POLL_INTERVAL")
    echo "Added poll interval to application arguments"
fi

if [ -n "$POLL_TIME_UNIT" ]; then
    application_arguments+=("--poll.timeunit=$POLL_TIME_UNIT")
    echo "Added poll time unit to application arguments"
fi

if [ -n "$REFRESH_INTERVAL" ]; then
    application_arguments+=("--refresh.interval=$REFRESH_INTERVAL")
    echo "Added refresh interval to application arguments"
fi

if [ -n "$REFRESH_TIME_UNIT" ]; then
    application_arguments+=("--refresh.timeunit=$REFRESH_TIME_UNIT")
    echo "Added refresh time unit to application arguments"
fi

if [ -n "$DEMO_MODE" ]; then
    application_arguments+=("--demo=$DEMO_MODE")
    echo "Added demo mode to application arguments"
fi

if [ -n "$HEADLESS_MODE" ]; then
    application_arguments+=("--headless=$HEADLESS_MODE")
    echo "Added headless mode to application arguments"
fi

java -jar /build/target/collector-service.jar "${application_arguments[@]}"