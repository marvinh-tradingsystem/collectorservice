<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.gitlab.marvinh</groupId>
    <artifactId>collector-service</artifactId>
    <version>3.0.1</version>
    <packaging>jar</packaging>

    <name>Collector Service</name>
    <description>This is library provides a service that collects product prices from web sources and stores them in a database</description>
    <url>https://gitlab.com/marvinh-tradingsystem/collectorservice</url>

    <licenses>
        <license>
            <name>MIT License</name>
            <url>https://opensource.org/licenses/MIT</url>
            <distribution>repo</distribution>
        </license>
    </licenses>
    <developers>
        <developer>
            <id>MH</id>
            <name>Marvin Haagen</name>
            <email>marvin.haagen@stud.htwk-leipzig.de</email>
            <roles>
                <role>architect</role>
                <role>developer</role>
            </roles>
            <timezone>Europe/Berlin</timezone>
        </developer>
    </developers>

    <properties>
        <maven.compiler.target>1.10</maven.compiler.target>
        <maven.compiler.source>1.10</maven.compiler.source>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <spring.boot.version>2.3.1.RELEASE</spring.boot.version>
        <spring.framework.version>5.2.7.RELEASE</spring.framework.version>
        <selenium.version>3.141.59</selenium.version>
    </properties>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>3.1.2</version>
                <configuration>
                    <usedDependencies>
                    </usedDependencies>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>${spring.boot.version}</version>
                <configuration>
                    <mainClass>service.collector.CollectorService</mainClass>
                    <layout>ZIP</layout>
                    <finalName>collector-service</finalName>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-webflux</artifactId>
            <version>${spring.boot.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <version>${spring.boot.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <version>${spring.boot.version}</version>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.junit.vintage</groupId>
                    <artifactId>junit-vintage-engine</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>com.gitlab.marvinh</groupId>
            <artifactId>java-plugins</artifactId>
            <version>2.0</version>
        </dependency>

        <dependency>
            <groupId>com.gitlab.marvinh</groupId>
            <artifactId>collector-plugins-api</artifactId>
            <version>1.0</version>
        </dependency>

        <dependency>
            <groupId>com.gitlab.marvinh</groupId>
            <artifactId>product</artifactId>
            <version>0.4</version>
        </dependency>

        <dependency>
            <groupId>com.gitlab.marvinh</groupId>
            <artifactId>prices-and-currencies</artifactId>
            <version>1.0</version>
        </dependency>

        <dependency>
            <groupId>org.seleniumhq.selenium</groupId>
            <artifactId>selenium-java</artifactId>
            <version>${selenium.version}</version>
        </dependency>
        <dependency>
            <groupId>org.seleniumhq.selenium</groupId>
            <artifactId>selenium-chrome-driver</artifactId>
            <version>${selenium.version}</version>
        </dependency>

        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>2.8.5</version>
        </dependency>

        <dependency>
            <groupId>net.sourceforge.htmlunit</groupId>
            <artifactId>htmlunit</artifactId>
            <version>2.42.0</version>
        </dependency>
    </dependencies>
</project>