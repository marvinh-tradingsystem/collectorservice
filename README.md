# CollectorService
This service collects data from URLs and other Resources and feeds them into a remote database provided by the database service.

## Command line arguments
The collector service provides following command line arguments:

### Mandatory command line arguments
* database.url: URL of the database service. For example http://localhost:8081
* query.url: URL to query the prices from. For example https://www.ls-tc.de/de
* plugin.file: Name and path of the JAR file containing the plugin for querying the prices
* product.file: Name and path of the file specifying the product to query
* currency.file: Name and path of the file specifying the currency the prices of the queryied product has

### Optional command line arguments
* collector.type: [Type of the collector](#valid-collector-types) to be used for querying the prices.
* poll.interval: Amount of time between the polls of the page. Only Integer values are allowed (must be a 64 bit signed Integer)
* poll.timeunit: [Time unit](#valid-time-units) of the poll interval.
* refresh.interval: Amount of time to pass before refreshing the page. Only Integer values are allowed (must be a 64 bit signed Integer)
* refresh.timeunit: [Time unit](#valid-time-units) of the page refresh interval.
* demo: [Boolean](#valid-boolean-values) value. If true, the Collector runs in demo mode in which it will not actually query web pages, but instead returns random generated prices.
* headless: [Boolean](#valid-boolean-values) value. If true, the browser will start in headless mode, so it will run in background and not open any window.

#### Valid collector types
* domPoll: Polls the DOM of the loaded page if anything has changed. This can be used for pages which are updated
in background by their own Javascript when new prices are available.

### Valid boolean values:
* true
* false

### Valid time units
* DAYS
* HOURS
* MINUTES
* SECONDS
* MILLISECONDS
* MICROSECONDS
* NANOSECONDS

## Docker environment variables
The docker container supports following environment variables:

### Mandatory Docker environment variables
* DATABASE_URL: Values is delegated as command line argument [database.url](#mandatory-command-line-arguments) 
* QUERY_URL: Values is delegated as command line argument [query.url](#mandatory-command-line-arguments) 
* PLUGIN_FILE: Values is delegated as command line argument [plugin.file](#mandatory-command-line-arguments) 
* PRODUCT_FILE: Values is delegated as command line argument [product.file](#mandatory-command-line-arguments) 
* CURRENCY_FILE: Values is delegated as command line argument [currency.file](#mandatory-command-line-arguments) 

### Optional Docker environment variables
* COLLECTOR_TYPE: Values is delegated as command line argument [collector.type](#optional-command-line-arguments)
* POLL_INTERVAL: Values is delegated as command line argument [poll.interval](#optional-command-line-arguments)
* POLL_TIME_UNIT: Values is delegated as command line argument [poll.timeunit](#optional-command-line-arguments)
* REFRESH_INTERVAL: Values is delegated as command line argument [refresh.interval](#optional-command-line-arguments)
* REFRESH_TIME_UNIT: Values is delegated as command line argument [refresh.timeunit](#optional-command-line-arguments)
* DEMO_MODE: Values is delegated as command line argument [demo](#optional-command-line-arguments)
* HEADLESS_MODE: Values is delegated as command line argument [headless](#optional-command-line-arguments)

## Docker mount points
The Docker container provides following mount points which can be used to mount directories from the host into the container:
* /mounts

## Docker ports
The Docker container exposes following ports:
* 5900: The VNC server that can be used for debugging the container and the used plugin