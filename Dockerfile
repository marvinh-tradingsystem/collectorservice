FROM debian:buster AS buildstage

# update the package manager cache and upgrade all packages
RUN apt-get -y update

# first setup tools needed for debugging
RUN apt-get --no-install-recommends -y install less

# now setup packages needed to run the application
RUN apt-get --no-install-recommends -y install openjdk-11-jdk
RUN apt-get --no-install-recommends -y install chromium chromium-driver

# last setup packages for building the application
RUN apt-get --no-install-recommends -y install maven

# now build the application
RUN mkdir /build

WORKDIR /build

COPY src/main/java/* /build/src/main/java/
COPY src/main/resources/* /build/src/main/resources/
COPY pom.xml /build/

RUN mvn clean package

# Now make a new layer containing only the necessary stuff to run the application
FROM debian:buster

RUN mkdir /mounts

RUN export DEBIAN_FRONTEND=noninteractive \
&& apt-get -y update \
&& apt-get --no-install-recommends -y install \
openjdk-11-jdk chromium chromium-driver locales \
openbox tigervnc-standalone-server supervisor gosu tint2 \
xdg-utils ca-certificates htop lxterminal feh \
&& rm -rf /var/lib/apt/lists/*

RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

COPY dockerfiles/etc/default/keyboard /etc/default/
COPY dockerfiles/etc/xdg/openbox/menu.xml /etc/xdg/openbox/
COPY dockerfiles/etc/xdg/openbox/wallpaper/* /etc/xdg/openbox/wallpaper/
COPY dockerfiles/etc/tint2/tint2.conf /etc/tint2/tint2.conf
COPY dockerfiles/etc/supervisord.conf /etc/
COPY dockerfiles/build/target/start.bash /build/target/

RUN mkdir -p /usr/share/desktop-directories \
&& chmod +x /build/target/start.bash

RUN groupadd --gid 1000 app \
&&  useradd --home-dir /data --shell /bin/bash --uid 1000 --gid 1000 app \
&&  mkdir -p /data

COPY --from=buildstage /build/target/collector-service.jar /build/target/
EXPOSE 5900
ENTRYPOINT chown app:app /data /dev/stdout && exec gosu app supervisord