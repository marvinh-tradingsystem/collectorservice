# This script can be called by the developer to build and deploy the docker image to docker hub
DOCKER_USER=mhaagen
VERSION=$(mvn org.apache.maven.plugins:maven-help-plugin:3.1.0:evaluate -Dexpression=project.version -q -DforceStdout)
echo "Image version is ${VERSION}"
docker login --username $DOCKER_USER
docker build -t oskar-collector-service .
docker tag oskar-collector-service $DOCKER_USER/oskar-collector-service
docker tag oskar-collector-service $DOCKER_USER/oskar-collector-service:$VERSION
docker push $DOCKER_USER/oskar-collector-service