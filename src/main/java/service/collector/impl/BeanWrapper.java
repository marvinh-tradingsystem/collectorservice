package service.collector.impl;

public class BeanWrapper<T> {
    private final T bean;

    public BeanWrapper(T bean) {
        this.bean = bean;
    }

    public T get() {
        return bean;
    }
}
