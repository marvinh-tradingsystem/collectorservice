package service.collector.impl.collector;

import collector.plugin.CollectorPlugin;
import collector.plugin.exception.ExtractionException;
import collector.plugin.exception.InitializationException;
import currency.Currency;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import price.Price;
import product.Product;
import product.ProductPrice;
import service.collector.CollectorConfigurationService;
import service.collector.CollectorStorage;
import service.collector.api.Collector;
import service.collector.impl.BeanWrapper;

import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.net.URL;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ScheduledThreadPoolExecutor;

@Service
public class DOMPollingCollector implements Collector {
    private class DomPoll implements Runnable{
        private final CollectorPlugin plugin;
        private final Product product;
        private final Currency currency;

        private DomPoll(CollectorPlugin plugin, Product product, Currency currency) {
            this.plugin = plugin;
            this.product = product;
            this.currency = currency;
        }

        @Override
        public void run() {
            try {
                Optional<ProductPrice> productPrice = extract();
                if (productPrice.isPresent()){
                    if (oldPrice == null || !Objects.equals(productPrice.get(), oldPrice)){
                        try {
                            collectorStorage.saveProductPrice(productPrice.get());
                            oldPrice = productPrice.get();
                        } catch (CollectorStorage.CollectorStorageException e) {
                            LOGGER.error(String.format("Could not save price:\n%s\n", e.getMessage()));
                        }
                    }
                }
                executor.schedule(new DomPoll(plugin, product, currency), pollInterval.get().getInterval(), pollInterval.get().getTimeUnit());
            } catch (ExtractionException exception) {
                throw new RuntimeException(exception);
            }
        }

        private Optional<ProductPrice> extract() throws ExtractionException {
            if (demoMode.get()){
                BigDecimal value = BigDecimal.valueOf(((new Random().nextInt() & Integer.MAX_VALUE) % 10000) / 100.0);
                return Optional.of(new ProductPrice(product, new Price(value, currency), ZonedDateTime.now()));
            }
            return plugin.extract(driver, product, currency, LoggerFactory.getLogger(plugin.getClass()));
        }
    }

    @Resource(name = "query.url")
    @Lazy
    private BeanWrapper<URL> queryURL;

    @Resource(name = "plugin")
    @Lazy
    private BeanWrapper<CollectorPlugin> collectorPlugin;

    @Resource(name = "product")
    @Lazy
    private BeanWrapper<Product> product;

    @Resource(name = "currency")
    @Lazy
    private BeanWrapper<Currency> currency;

    @Resource(name = "collector.demo")
    @Lazy
    private BeanWrapper<Boolean> demoMode;

    @Resource(name = "collector.headless")
    @Lazy
    private BeanWrapper<Boolean> headless;

    @Resource(name = "collector.page.refresh")
    @Lazy
    private BeanWrapper<Optional<Collector.Interval>> refreshInterval;

    @Resource(name = "collector.poll.interval")
    @Lazy
    private BeanWrapper<Interval> pollInterval;

    private final Logger LOGGER = LoggerFactory.getLogger(DOMPollingCollector.class);

    private WebDriver driver;
    private ProductPrice oldPrice;

    private final ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);

    private final CollectorConfigurationService collectorConfigurationService;
    private final CollectorStorage collectorStorage;

    @Autowired
    public DOMPollingCollector(CollectorConfigurationService configurationService, CollectorStorage collectorStorage) {
        this.collectorConfigurationService = configurationService;
        this.collectorStorage = collectorStorage;
    }

    @Override
    public void init() throws InitializationException {
        if (driver != null){
            driver.quit();
            driver = null;
        }

        assert collectorPlugin.get() != null;
        assert product.get() != null;
        assert currency.get() != null;

        if (!demoMode.get()){
            ChromeOptions options = new ChromeOptions();
            if (headless.get()){
                options.setHeadless(true);
            }
            options.addArguments("--no-sandbox");
            driver = new ChromeDriver(options);
            driver.manage().window().maximize();

            driver.get(queryURL.get().toString());

            collectorPlugin.get().init(driver, product.get(), currency.get(), LoggerFactory.getLogger(collectorPlugin.get().getClass()));
        }
        new DomPoll(collectorPlugin.get(), product.get(), currency.get()).run();

        if (refreshInterval.get().isPresent()){
            executor.schedule(() -> {
                try {
                    init();
                } catch (InitializationException e) {
                    throw new RuntimeException(e);
                }
            }, refreshInterval.get().get().getInterval(), refreshInterval.get().get().getTimeUnit());
            LOGGER.info(String.format("Scheduled reinitialization in %d %s", refreshInterval.get().get().getInterval(), refreshInterval.get().get().getTimeUnit().name()));
        }
        LOGGER.info("Collector started successfully.");
    }

    @PreDestroy
    @Override
    public void close() {
        executor.shutdown();
        if (driver != null){
            driver.quit();
        }
    }
}
