package service.collector.impl.database.request;

public class Price {
    private String value;
    private Currency currency;

    public Price() {}

    public Price(String value, Currency currency) {
        this.value = value;
        this.currency = currency;
    }

    public String getValue() {
        return value;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
