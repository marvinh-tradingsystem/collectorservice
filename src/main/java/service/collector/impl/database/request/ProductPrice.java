package service.collector.impl.database.request;

public class ProductPrice {
    private Product product;
    private Price price;
    private String zonedDateTime;

    public ProductPrice() {}

    public ProductPrice(Product product, Price price, String zonedDateTime) {
        this.product = product;
        this.price = price;
        this.zonedDateTime = zonedDateTime;
    }

    public Product getProduct() {
        return product;
    }

    public Price getPrice() {
        return price;
    }

    public String getDateTime() {
        return zonedDateTime;
    }

    public void setZonedDateTime(String zonedDateTime) {
        this.zonedDateTime = zonedDateTime;
    }
}
