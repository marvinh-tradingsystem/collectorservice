package service.collector;

import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.google.gson.Gson;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import product.ProductPrice;
import service.collector.impl.BeanWrapper;
import service.collector.impl.database.request.Currency;
import service.collector.impl.database.request.Price;
import service.collector.impl.database.request.Product;

import javax.annotation.Resource;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.format.DateTimeFormatter;

@Service
public class CollectorStorage {
    public static class CollectorStorageException extends Exception {
        public CollectorStorageException(String message) {
            super(message);
        }

        public CollectorStorageException(Throwable cause) {
            super(cause);
        }
    }

    @Resource(name = "database.url")
    @Lazy
    private BeanWrapper<URL> databaseURL;

    public void saveProductPrice(ProductPrice productPrice) throws CollectorStorageException {
        Currency currency = new Currency();
        currency.setAbbreviation(productPrice.getPrice().getCurrency().getAbbreviation());
        currency.setName(productPrice.getPrice().getCurrency().getName());
        currency.setSymbol(productPrice.getPrice().getCurrency().getSymbol().get());

        Price price = new Price();
        price.setValue(productPrice.getPrice().getValue().toString());
        price.setCurrency(currency);

        Product product = new Product();
        product.setName(productPrice.getProduct().getName());
        service.collector.impl.database.request.ProductPrice requestProductPrice
                = new service.collector.impl.database.request.ProductPrice(
                        product, price, productPrice.getZonedDateTime().format(DateTimeFormatter.ISO_ZONED_DATE_TIME)
        );

        try{
            WebClient webClient = new WebClient();
            WebRequest request = new WebRequest(new URL(databaseURL.get(), "/product/price/store"), HttpMethod.POST);
            String requestBody = new Gson().toJson(requestProductPrice, service.collector.impl.database.request.ProductPrice.class);
            request.setRequestBody(requestBody);
            request.setCharset(StandardCharsets.UTF_8);
            request.setAdditionalHeader("Content-Type", "application/json;charset=UTF-8");
            WebResponse webResponse = webClient.getPage(request).getWebResponse();
            if (webResponse.getStatusCode() != 200) {
                throw new CollectorStorageException(String.format("Request failed: %s", webResponse.getContentAsString()));
            }
        } catch (IOException | RuntimeException exception){
            throw new CollectorStorageException(exception);
        }
    }
}
