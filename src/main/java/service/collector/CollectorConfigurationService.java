package service.collector;

import collector.plugin.CollectorPlugin;
import currency.Currency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import product.Product;
import service.collector.api.Collector;
import service.collector.impl.BeanWrapper;

import java.net.URL;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Lazy
@Service
@Configuration
public class CollectorConfigurationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CollectorConfigurationService.class);

    private CollectorPlugin collectorPlugin = null;
    private Product product = null;
    private Currency currency = null;

    private URL databaseURL = null;
    private URL queryURL = null;
    private Collector.Interval pollInterval = new Collector.Interval() {
        @Override
        public long getInterval() {
            return 100;
        }

        @Override
        public TimeUnit getTimeUnit() {
            return TimeUnit.MILLISECONDS;
        }
    };
    private Collector.Interval refreshInterval = null;

    private boolean demoMode = false;
    private boolean headless = false;

    @Bean(name = "plugin")
    public BeanWrapper<CollectorPlugin> getCollectorPlugin(){
        return new BeanWrapper<>(collectorPlugin);
    }

    public void setCollectorPlugin(CollectorPlugin collectorPlugin){
        this.collectorPlugin = collectorPlugin;
        LOGGER.info(String.format("Plugin set: %s", collectorPlugin.getName()));
    }

    @Bean(name = "product")
    public BeanWrapper<Product> getProduct(){
        return new BeanWrapper<>(product);
    }

    public void setProduct(Product product){
        this.product = product;
        LOGGER.info(String.format("Product set: %s", product.getName()));
    }

    @Bean(name = "currency")
    public BeanWrapper<Currency> getCurrency(){
        return new BeanWrapper<>(currency);
    }

    public void setCurrency(Currency currency){
        this.currency = currency;
        LOGGER.info(String.format("Currency set: %s", currency.getName()));
    }

    @Bean(name = "database.url")
    public BeanWrapper<URL> databaseURL(){
        return new BeanWrapper<>(databaseURL);
    }

    public void setDatabaseURL(URL databaseURL){
        this.databaseURL = databaseURL;
        LOGGER.info(String.format("Database URL set: %s", databaseURL));
    }

    @Bean(name = "query.url")
    public BeanWrapper<URL> getQueryURL() {
        return new BeanWrapper<>(queryURL);
    }

    public void setQueryURL(URL queryURL) {
        this.queryURL = queryURL;
        LOGGER.info(String.format("Query URL set: %s", queryURL));
    }

    @Bean(name = "collector.poll.interval")
    public BeanWrapper<Collector.Interval> getPollInterval(){
        return new BeanWrapper<>(pollInterval);
    }

    public void setPollInterval(Collector.Interval pollInterval){
        this.pollInterval = pollInterval;
        LOGGER.info(String.format("Poll interval set: %d %s", pollInterval.getInterval(), pollInterval.getTimeUnit().name().toLowerCase()));
    }

    @Bean(name = "collector.page.refresh")
    public BeanWrapper<Optional<Collector.Interval>> getRefreshInterval(){
        return new BeanWrapper<>(Optional.ofNullable(this.refreshInterval));
    }

    public void setRefreshInterval(Collector.Interval refreshInterval){
        this.refreshInterval = refreshInterval;
        LOGGER.info(String.format("Refresh interval set: %d %s", refreshInterval.getInterval(), refreshInterval.getTimeUnit().name().toLowerCase()));
    }

    @Bean(name = "collector.demo")
    public BeanWrapper<Boolean> demo(){
        return new BeanWrapper<>(demoMode);
    }

    public void setDemoMode(){
        this.demoMode = true;
        LOGGER.info("Demo mode activated");
    }

    @Bean(name = "collector.headless")
    public BeanWrapper<Boolean> headless(){
        return new BeanWrapper<>(headless);
    }

    public void setHeadless(){
        this.headless = true;
        LOGGER.info("Headless mode activated");
    }
}
