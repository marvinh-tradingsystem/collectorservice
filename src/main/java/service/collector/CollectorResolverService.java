package service.collector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import service.collector.api.Collector;
import service.collector.impl.BeanWrapper;
import service.collector.impl.collector.DOMPollingCollector;

import javax.annotation.Resource;
import java.net.URL;

@Lazy
@Service
public class CollectorResolverService {
    public enum CollectorType{
        DOMPollingCollector("domPoll")
        ;

        private final String typeString;

        CollectorType(String typeString) {
            this.typeString = typeString;
        }

        public String getTypeString() {
            return typeString;
        }
    }

    @Resource(name = "database.url")
    @Lazy
    private BeanWrapper<URL> databaseURL;

    private final DOMPollingCollector domPollingCollector;

    @Autowired
    public CollectorResolverService(
            DOMPollingCollector domPollingCollector
    ) {
        this.domPollingCollector = domPollingCollector;
    }

    public Collector resolveCollectorByType(String type) {
        if (type.equals(CollectorType.DOMPollingCollector.typeString)){
            return domPollingCollector;
        }
        throw new RuntimeException(String.format("Unknown collector type: %s", type));
    }
}
