package service.collector;

import collector.plugin.CollectorPlugin;
import com.google.gson.Gson;
import currency.Currency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import pluginloader.PluginLoader;
import product.Product;
import service.collector.api.Collector;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.util.jar.JarInputStream;

@Lazy
@Configuration
@ServletComponentScan
@SpringBootApplication
public class CollectorService implements ApplicationRunner {
    @Value("${database.url:}")
    private String databaseServiceURL;

    @Value("${collector.type:domPoll}")
    private String collectorType;

    @Value("${query.url:}")
    private String queryURL;

    @Value("${plugin.file:}")
    private String pluginFile;

    @Value("${product.file:}")
    private String productFile;

    @Value("${currency.file:}")
    private String currencyFile;

    @Value("${poll.interval:}")
    private String pollInterval;

    @Value("${poll.timeunit:MILLISECONDS}")
    private String pollTimeUnit;

    @Value("${refresh.interval:}")
    private String refreshInterval;

    @Value("${refresh.timeunit:HOURS}")
    private String refreshTimeUnit;

    @Value("${demo:false}")
    private String demoMode;

    @Value("${headless:false}")
    private String headless;

    private final Logger LOGGER = LoggerFactory.getLogger(CollectorService.class);

    private final CollectorResolverService collectorResolverService;
    private final CollectorConfigurationService collectorConfigurationService;

    private static class CurrencyFile{
        private String abbreviation;
        private String name;
        private String symbol;
    }

    private static class ProductFile{
        private String name;
    }

    public static void main(String[] args){
        SpringApplication.run(CollectorService.class, args);
    }

    @Autowired
    public CollectorService(CollectorResolverService collectorResolverService,
                            CollectorConfigurationService configurationService) {
        this.collectorResolverService = collectorResolverService;
        this.collectorConfigurationService = configurationService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        LOGGER.info(String.format("Current working directory: %s", System.getProperty("user.dir")));

        try{
            URL databaseURL = new URL(databaseServiceURL);
            collectorConfigurationService.setDatabaseURL(databaseURL);
        } catch (MalformedURLException exception){
            LOGGER.error(String.format("Invalid database URL: '%s'", databaseServiceURL));
            System.exit(-1);
        }

        try{
            URL queryURL = new URL(this.queryURL);
            collectorConfigurationService.setQueryURL(queryURL);
        } catch (MalformedURLException exception){
            LOGGER.error(String.format("Invalid query URL: '%s'", this.queryURL));
            System.exit(-1);
        }

        File collectorPluginFile = new File(this.pluginFile);
        if (!collectorPluginFile.exists()){
            LOGGER.error(String.format("Plugin file could not be found: '%s'", pluginFile));
            System.exit(-1);
        }
        PluginLoader pluginLoader = new PluginLoader();
        CollectorPlugin collectorPlugin = pluginLoader.load(new JarInputStream(new FileInputStream(collectorPluginFile)), CollectorPlugin.class);
        collectorConfigurationService.setCollectorPlugin(collectorPlugin);

        File productFile = new File(this.productFile);
        if (!productFile.exists()){
            LOGGER.error(String.format("Product file does not exist: '%s'", productFile));
            System.exit(-1);
        }
        Product product = parseProduct(new Gson().fromJson(new FileReader(productFile), ProductFile.class));
        collectorConfigurationService.setProduct(product);

        File currencyFileObject = new File(currencyFile);
        if (!currencyFileObject.exists()){
            LOGGER.error(String.format("Currency file does not exist: '%s'", currencyFileObject));
            System.exit(-1);
        }
        CurrencyFile parsedCurrencyFile = new Gson().fromJson(new FileReader(currencyFileObject), CurrencyFile.class);
        Currency currency = new Currency(
                parsedCurrencyFile.name,
                new Currency.Symbol(parsedCurrencyFile.symbol),
                parsedCurrencyFile.abbreviation
        );
        collectorConfigurationService.setCurrency(currency);

        if (!pollInterval.isEmpty() && !pollTimeUnit.isEmpty()){
            collectorConfigurationService.setPollInterval(new Collector.Interval() {
                @Override
                public long getInterval() {
                    return Long.parseLong(pollInterval);
                }

                @Override
                public TimeUnit getTimeUnit() {
                    return TimeUnit.valueOf(pollTimeUnit);
                }
            });
        }

        if (!refreshInterval.isEmpty() && !refreshTimeUnit.isEmpty()){
            collectorConfigurationService.setRefreshInterval(new Collector.Interval() {
                @Override
                public long getInterval() {
                    return Long.parseLong(refreshInterval);
                }

                @Override
                public TimeUnit getTimeUnit() {
                    return TimeUnit.valueOf(refreshTimeUnit);
                }
            });
        }

        if (Boolean.parseBoolean(demoMode)){
            collectorConfigurationService.setDemoMode();
        }

        if (Boolean.parseBoolean(headless)){
            collectorConfigurationService.setHeadless();
        }

        collectorResolverService.resolveCollectorByType(collectorType).init();
    }

    private Product parseProduct(ProductFile productFile){
        return () -> productFile.name;
    }
}
