package service.collector.api;

import collector.plugin.exception.InitializationException;

import java.util.concurrent.TimeUnit;

/**
 * The Collector collects product price data from external URLS.
 * Every Collector supports a demo mode. If the demo mode is enabled via
 * the flag Flags.COLLECTOR_DEMO_MODE_ON the DemoCollectorPlugin will
 * be used which randomly generates product prices so the system can
 * be tested without a working internet connection.
 */
public interface Collector extends AutoCloseable{
    interface Interval {
        long getInterval();
        TimeUnit getTimeUnit();
    }

    void init() throws InitializationException;
}
